/**
 * Applcation to be launched from here.
 **/
$(document).ready(function() {
    Epson.Controller = {
        _opednedGlobal: "",
        _videoPlayer: null,
        init: function() {
            this.bindEvents();
            this._initVideo();
        },
        _initVideo: function() {

            var videoFn = function() {
                if ($("#example_video_1").length > 0)
                {
                    this._videoPlayer = videojs('example_video_1');
                    this._videoPlayer.on("play", $.proxy(this._onVideoStarted, this));
                    this._videoPlayer.on("ended", $.proxy(this._onVideoPausedOrEnded, this));
                    this._videoPlayer.on("pause", $.proxy(this._onVideoPausedOrEnded, this));
                }
            };

            setTimeout($.proxy(videoFn, this), 5000);

        },
        /**
         * Bind all the global events here. This function makes sure that all the dom element exists
         * @returns {undefined}
         */
        bindEvents: function() {
            // Animatoin of page when clicked on anchor
            $(".nav_page").anchorAnimate();

            // Pagination scrips
            // Animation for slides
            $('#slides').slides({
                preloadImage: 'img/loading.gif',
                play: 0,
                pause: 2500,
                hoverPause: true,
                animationStart: function(current) {
                    $('.caption').animate({
                        bottom: -35
                    }, 100);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationStart on slide: ', current);
                    }
                    ;
                },
                animationComplete: function(current) {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationComplete on slide: ', current);
                    }
                    ;
                },
                slidesLoaded: function() {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                }
            });


            // Animation
            $('#slider2').slides({
                preloadImage: 'img/loading.gif',
                play: 0,
                animationStart: function(current) {
                    $('.caption').animate({
                        bottom: -35
                    }, 100);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationStart on slide: ', current);
                    }
                    ;
                },
                animationComplete: function(current) {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationComplete on slide: ', current);
                    }
                    ;
                },
                slidesLoaded: function() {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                }
            });


            // will live in closure
            var me = this;

            // Hiding modals when esc is pressed
            $(document).keydown(function(e) {
                if (e.keyCode === 27) {
                    $('.modal').remove();
                    $('.rmClass').fadeOut(200);
                    me._opednedGlobal = "";
                }
            });

            $(".popup-over").on("mouseover", $.proxy(function(e) {
                var jTarget = $(e.target);
                this.openPopup(jTarget.data("popup_id"), jTarget.data("popup_type"));
            }, this));

            $(".popup").on("click", $.proxy(function(e) {
                var jTarget = $(e.target);
                this.openPopup(jTarget.data("popup_id"), jTarget.data("popup_type"));
            }, this));

            $(".close__pop").on("click", function(e) {
                var jTarget = $(e.target), jPopupBlock = jTarget.closest(".popup_block");
                $('.modal').remove();
                jPopupBlock.fadeOut(200);
                me._opednedGlobal = "";
            });

            $(function() {
                $("#g1").jFlip(333, 519, {cornersTop: true});
            });

            $(window).on('DOMContentLoaded load resize scroll', $.proxy(this._onScrollResize, this));
            $("#contact-form").on("submit", function(e) {
                var jErrorMsgEl = $("#trailFormErrMessage");
                jErrorMsgEl.html("");
                var companyName = $.trim($("#companyName").val());
                var userName = $.trim($("#userName").val());
                var companyAddress = $.trim($("#companyAddress").val());
                var designation = $.trim($("#designation").val());
                var workIndustry = $.trim($("#workIndustry").val());
                var contactNumber = $.trim($("#contactNumber").val());
                var staffStrength = $.trim($("#staff-strength").val());
                var email = $.trim($("#email").val());
                var prefModel = $.trim($("#pref-model").val());

                var captcha = $.trim($("#captcha").val());
                var staffStrength = $.trim($("#staff-strength").val());
                var prefModel = $.trim($("#pref-model").val());

                if (companyName === "" || userName === "" || companyAddress === "" || contactNumber === "" || prefModel === "" || staffStrength === "") {
                    jErrorMsgEl.html("Fields marked with * are mandatory");
                } else if ((companyName.indexOf(",") >= 0) || (userName.indexOf(",") >= 0) || (companyAddress.indexOf(",") >= 0) || (designation.indexOf(",") >= 0) || (contactNumber.indexOf(",") >= 0) || (email.indexOf(",") >= 0)) {
                    jErrorMsgEl.html("Please don't use \",\"");
                }
                else if (!isValidEmailAddress(email)) {
                    jErrorMsgEl.html("You have not given a correct e-mail address");
                } else if (!$("#disclaimer-checked").prop("checked")) {
                    jErrorMsgEl.html("Please agree to the terms to continue");
                } else if (captcha === "") {
                    jErrorMsgEl.html("Please enter the captcha code");
                } else if (captcha !== randStr) {
                    jErrorMsgEl.html("Captcha code didn't match");
                }
                if (jErrorMsgEl.html().length === 0)
                {
                    $.ajax({
                        url: "/webservice/save-form.jsp",
                        context: document.body,
                        data: $(this).serialize(),
                        type: "POST"
                    }).done(function(response)
                    {
                        jErrorMsgEl.html("Form successfully submitted");
                    }).error(function(response)
                    {
                    });
                    return false;
                }
                else
                {
                    return false;
                }
            });
            // captcha when document get reload
            $(document).ready(function() {
                var randStr = '';
                $("#refresh_captcha").click();
            });

        },
        _onScrollResize: function() {
            var jarrEls = $(".nav-block");
            var iLooper = 0, iLen = jarrEls.length, el;
            for (; iLooper < iLen; iLooper++) {
                el = jarrEls[iLooper];
                if (isElementInViewport(el)) {
                    this._onEnterViewPort($(el));
                    break;
                }
            }

        },
        _onEnterViewPort: function(jEl) {
            var strId = jEl.attr("id");
            var currentActive = $(".nav_page.active");
            var jTargetLink = $(".nav_page[href='#" + strId + "']");
            if (jTargetLink === currentActive)
            {
                return;
            }

            $(".nav_page").removeClass("active");
            jTargetLink.addClass("active");

        },
        /**
         * Opens popup given the popup id
         * @param {type} popId
         * @param {type} ifNotMdl
         * @returns {undefined}
         */
        openPopup: function(popId, ifNotMdl) {
            if (ifNotMdl != 1) {
                $('body').append('<div class="modal"></div>');
            }

            if (this._opednedGlobal != popId) {
                $("#" + this._opednedGlobal).fadeOut(200);
                $("#" + popId).fadeIn(400);
                this._opednedGlobal = popId;
            }
        },
        /**
         * Event handling below
         */
        _onVideoStarted: function() {
            $(".interview__video--text").fadeOut();
        },
        _onVideoPausedOrEnded: function() {
            $(".interview__video--text").fadeIn();
        }
    };
    // Initialize view controller to bind events if any
    Epson.Controller.init();
});

function isElementInViewport(el) {

    //special bonus for those using jQuery
    if (el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
            );
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}
;
function randomString() {
    // Genrate random string
    randStr = '';
    var chars = "0123456789ABCDEFGHIJKL!@#$%^&*()MNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 5;  //Length of Captcha
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randStr += chars.substring(rnum, rnum + 1);
    }
    // Getting canvas variable
    var tCtx = document.getElementById('textCanvas').getContext('2d'),
            imageElem = document.getElementById('image');
    tCtx.font = "normal 24px Verdana";  //set text fonts option for  canvas,html5 canvas: Text Option
    tCtx.strokeStyle = "#000000";    //html5 canvas: Text Option
    tCtx.clearRect(0, 0, 100, 40);        //Clear previous Canvas for redraw our captcha
    tCtx.strokeText(randStr, 30, 20, 70);  //Stroke random string to canvas
    tCtx.textBaseline = "middle";                  //html5 canvas: Text Option,line in middle of text
    imageElem.src = tCtx.canvas.toDataURL(); // provide canvas url to image src
    console.log(imageElem.src);  //image
}
/*******
 
 ***	Anchor Slider by Cedric Dugas   ***
 *** Http://www.position-absolute.com ***
 
 Never have an anchor jumping your content, slide it.
 
 Don't forget to put an id to your anchor !
 You can use and modify this script for any project you want, but please leave this comment as credit.
 
 *****/
jQuery.fn.anchorAnimate = function(settings) {

    settings = jQuery.extend({
        speed: 1100
    }, settings);

    return this.each(function() {
        var caller = this;
        $(caller).click(function(event) {
            event.preventDefault();
            var locationHref = window.location.href;
            var elementClick = $(caller).attr("href");
            $(".nav_page").removeClass("active");
            //$(caller).addClass("active");
            var destination = $(elementClick).offset().top;
            $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, settings.speed, function() {
                window.location.hash = elementClick;
            });
            return false;
        })
    })
};