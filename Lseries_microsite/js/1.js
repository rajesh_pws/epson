// Define some variables used to remember state.
    var nextPageToken, prevPageToken;
    var content = '';
    //var playlistId = youtube_video_play_list_id = localStorage.getItem('youtube_video_play_list_id');
    // Retrieve the next page of videos in the playlist.
     function nextPage() {
       requestVideoPlaylist(nextPageToken);
     }

     // Retrieve the previous page of videos in the playlist.
     function previousPage() {
       requestVideoPlaylist(prevPageToken);
     }
     // Retrieve the list of videos in the specified playlist.
    function requestVideoPlaylist(pageToken) {
	   $(".EpsonLoader").show();
       $('#YouTubePlayList').html('');
       var content = '';
      apirequestOptionsString = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId="+channel_id+"&maxResults="+no_of_playlists_per_page+"&order=date&key="+ api_key +"&alt=json";
      if (pageToken) {
        apirequestOptionsString = apirequestOptionsString + "&pageToken="+pageToken;
      }
      
      $.getJSON(apirequestOptionsString, function (data) {
		  
            // + converts the string to int
            var video = data.items;
            // Only show pagination buttons if there is a pagination token for the
            // next or previous page of results.
            nextPageToken = data.nextPageToken;
            var nextVis = nextPageToken ? 'block' : 'none';
            $('#next-button').css('display', nextVis);
            prevPageToken = data.prevPageToken
            var prevVis = prevPageToken ? 'block' : 'none';
            $('#prev-button').css('display', prevVis);

            var result = $.map(video, function(value, key) { 
                if (value['snippet']['title'].length > 86) {
                    title = value['snippet']['title'].substr(0, 83) + "...";
                } else {
                    title = value['snippet']['title'];
                }
               // video_album_full_description = value['snippet']['description'];
                if (title == '')    title = '';
                content += '<li id="'+value['id']+'">'+
                    '<div class="vidFrame">' +
                        '<img src="' + value['snippet']['thumbnails']['medium']['url'] + '" alt="" title="" border="0" />' +
                    '</div>' +
                     '<h3>' + title + '</h3>' +
                     //'<span>' + new Date(value['snippet']['publishedAt'].substr(0, 19)).toDateString() + '</span>' +
                     // "<p data-rel='"+ escape(video_album_full_description) +"'>" + des + '</p>' +
                '</li>';
                });
            
            $("#YouTubePlayList").html(content);
            $(".EpsonLoader").hide();
           // $(".group-text-button h1").html(title);
           // $(".mrg-b39").html(unescape(pl_description));
        });
      
    }

$(function() {
		$.getJSON("https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId="+channel_id+"&maxResults="+no_of_playlists_per_page+"&order=date&key="+ api_key +"&alt=json",function(data) {
            // + converts the string to int
            var video = data.items;
            var content = '';
            var video_album_full_description = '';

            // Only show pagination buttons if there is a pagination token for the
            // next or previous page of results.
            nextPageToken = data.nextPageToken;
            var nextVis = nextPageToken ? 'block' : 'none';
            $('#next-button').css('display', nextVis);
            prevPageToken = data.prevPageToken
            var prevVis = prevPageToken ? 'block' : 'none';
            $('#prev-button').css('display', prevVis);


            var result = $.map(video, function(value, key) { 
				if (value['snippet']['title'].length > 86) {
                    title = value['snippet']['title'].substr(0, 83) + "...";
                } else {
                    title = value['snippet']['title'];
                }
               // video_album_full_description = value['snippet']['description'];
                if (title == '')  title = '';
                content += '<li id="'+value['id']+'">'+
                    '<div class="vidFrame">' +
                        '<img src="' + value['snippet']['thumbnails']['medium']['url'] + '" alt="" title="" border="0" />' +
                    '</div>' +
                     '<h3>' + title + '</h3>' +
                     //'<span>' + new Date(value['snippet']['publishedAt'].substr(0, 19)).toDateString() + '</span>' +
                     // "<p data-rel='"+ escape(video_album_full_description) +"'>" + des + '</p>' +
                '</li>';
				});
            $("#YouTubePlayList").html(content);
            $(".EpsonLoader").hide();
    });

   $(document).on("click", "#YouTubePlayList li", function (event) {
        var v_album_id = $(this).attr('id');
        localStorage.setItem('youtube_video_play_list_id',$(this).attr('id'));
        var vt = $('#'+v_album_id+' h3').html();
        //var vd = $('#'+v_album_id+' p').data('rel');
        localStorage.setItem('vtitle', vt);
       // localStorage.setItem('vdescription', vd);
		document.location = "video_gallery_details.html";

   });
});   
   
