    // Define some variables used to remember state.
    var playlistId, nextPageToken, prevPageToken;
    var content = '';
    var playlistId = youtube_video_play_list_id = localStorage.getItem('youtube_video_play_list_id');
    // Retrieve the next page of videos in the playlist.
     function nextPage() {
       requestVideoPlaylist(playlistId, nextPageToken);
     }

     // Retrieve the previous page of videos in the playlist.
     function previousPage() {
       requestVideoPlaylist(playlistId, prevPageToken);
     }
     // Retrieve the list of videos in the specified playlist.
    function requestVideoPlaylist(playlistId, pageToken) {
	   $(".EpsonLoader").show();	
       $('#YouTubePlayList').html('');
       var content = '';
      apirequestOptionsString = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&channelId="+channel_id+"&playlistId=" + playlistId + "&key=" + api_key + "&maxResults="+no_of_videos_in_playlist_per_page+"&alt=json";
      if (pageToken) {
        apirequestOptionsString = apirequestOptionsString + "&pageToken="+pageToken;
      }
      
      $.getJSON(apirequestOptionsString, function (data) {
		 
            // + converts the string to int
            var info = data.items;
            // Only show pagination buttons if there is a pagination token for the
            // next or previous page of results.
            nextPageToken = data.nextPageToken;
            var nextVis = nextPageToken ? 'block' : 'none';
            $('#next-button').css('display', nextVis);
            prevPageToken = data.prevPageToken
            var prevVis = prevPageToken ? 'block' : 'none';
            $('#prev-button').css('display', prevVis);

            var result = $.map(info, function (value, key) {
		if (value.snippet.title.length > 86) {
                    title = value.snippet.title.substr(0, 83) + "...";
                } else {
                    title = value.snippet.title;
                }
                if (title == '')   title = '';
                content += '<li>' +
                      '<div class="vidFrame">' +
                          '<a class="youtube" href="https://www.youtube.com/embed/' + value.snippet.resourceId.videoId + '"><img src="' + value.snippet.thumbnails.medium['url'] + '"/></a>' +
                      '</div>' +
                      '<h3>' + title + '</h3>' +
                      // '<span>' + new Date(value['published']['$t']).toDateString() + '</span>' +
                      // '<p>' + des + '</p>' +
                  '</li>';

            });
            $("#YouTubePlayList").html(content);
            
            if ($(window).width() > 767) {
                $(".youtube").colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
            }
            $(".EpsonLoader").hide();
           // $(".group-text-button h1").html(title);
           // $(".mrg-b39").html(unescape(pl_description));
        });
      
    }
$(function () {

        var title = localStorage.getItem('vtitle');
        //var pl_description = localStorage.getItem('vdescription');
        console.log(youtube_video_play_list_id);
        $.getJSON("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&channelId="+channel_id+"&playlistId=" + youtube_video_play_list_id + "&key=" + api_key + "&maxResults="+no_of_videos_in_playlist_per_page+"&alt=json", function (data) {
            // + converts the string to int
            var info = data.items;
            // Only show pagination buttons if there is a pagination token for the
            // next or previous page of results.
            nextPageToken = data.nextPageToken;
            var nextVis = nextPageToken ? 'block' : 'none';
            $('#next-button').css('display', nextVis);
            prevPageToken = data.prevPageToken;
            var prevVis = prevPageToken ? 'block' : 'none';
            $('#prev-button').css('display', prevVis);

            var result = $.map(info, function (value, key) {
		if (value.snippet.title.length > 86) {
                    title = value.snippet.title.substr(0, 83) + "...";
                } else {
                    title = value.snippet.title;
                }
                if (title == '')   title = '';
                content += '<li>' +
                      '<div class="vidFrame">' +
                          '<a class="youtube" href="https://www.youtube.com/embed/' + value.snippet.resourceId.videoId + '"><img src="' + value.snippet.thumbnails.medium['url'] + '"/></a>' +
                      '</div>' +
                      '<h3>' + title + '</h3>' +
                      // '<span>' + new Date(value['published']['$t']).toDateString() + '</span>' +
                      // '<p>' + des + '</p>' +
                  '</li>';

            });
            $("#YouTubePlayList").html(content);
            
            if ($(window).width() > 767) {
                $(".youtube").colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
            }
            $(".EpsonLoader").hide();
           // $(".group-text-button h1").html(title);
           // $(".mrg-b39").html(unescape(pl_description));
        });

});

