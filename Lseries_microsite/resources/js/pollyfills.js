Array.prototype.removeByAttr = function(attr, matchVal) {
    for (var i = 0; i < this.length; i++) {
        if ((this[i])[attr] == matchVal) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
}



if (window.Float32Array === undefined || window.Float32Array === null) {
    window.Float32Array = Array;
    Float32Array.prototype.setDataValue = function(data) {
        this.set(data);
    };

    Array.prototype.set = function(data) {
        this.length = 0;
        var iLooper = 0, iLen = data.length;

        for (; iLooper < iLen; iLooper++) {
            this.push(data[iLooper]);
        }
    };

}


if (window.Float32Array)
{
    Float32Array.prototype.setDataValue = function(data) {
        this.set(data);
    };
}

Array.prototype.setDataValue = function(data) {
    this.set(data);
};

Array.isArray||(Array.isArray=function(a){return''+a!==a&&{}.toString.call(a)=='[object Array]'});

Object.keys=Object.keys||function(o,k,r){r=[];for(k in o)r.hasOwnProperty.call(o,k)&&r.push(k);return r}

if(!Array.prototype.forEach){Array.prototype.forEach=function(e,t){var n,r;if(this==null){throw new TypeError(" this is null or not defined")}var i=Object(this);var s=i.length>>>0;if(typeof e!=="function"){throw new TypeError(e+" is not a function")}if(arguments.length>1){n=t}r=0;while(r<s){var o;if(r in i){o=i[r];e.call(n,o,r,i)}r++}}}