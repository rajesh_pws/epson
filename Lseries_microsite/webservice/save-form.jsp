<%@ page import="java.io.*,java.util.*" %>
<%@ page import="javax.servlet.*,java.text.*" %>
<%
    response.addHeader("Access-Control-Allow-Origin", "*");
    final String companyName = request.getParameter("companyName");
    final String userName = request.getParameter("userName");
    final String companyAddress = request.getParameter("companyAddress");
    final String designation = request.getParameter("designation");
    final String workIndustry = request.getParameter("workIndustry");
    final String contactNumber = request.getParameter("contactNumber");
    final String staffStrength = request.getParameter("staff-strength");
    final String email = request.getParameter("email");
    final String prefModel = request.getParameter("pref-model");
    
    Date dNow = new Date();
    SimpleDateFormat ft = 
    new SimpleDateFormat ("E dd.MM.yyyy 'at' hh:mm:ss a zzz");
    final String strDate = ft.format(dNow);
    
    /*
    String finalMsg = "First Name : " + companyName  + "<br/>";
    finalMsg = finalMsg +  "Last Name : " + userName + "<br/>";
    finalMsg = finalMsg +  "Mobile : " + mobile + "<br/>";
    finalMsg = finalMsg +  "Country : " + country + "<br/>";
    finalMsg = finalMsg +  "Email : " + email + "<br/>";
    finalMsg = finalMsg +  "Phone: " + phone + "<br/>";
    finalMsg = finalMsg +  "Staff Strength: " + staffStrength + "<br/>";
    finalMsg = finalMsg +  "Preferred Model : " + prefModel + "<br/>";
    finalMsg = finalMsg +  "Work Industry: " + workIndustry + "<br/>";
    finalMsg = finalMsg + "-------------------------------------------------------------------------";
    finalMsg = finalMsg + "<br/><br/>";
    */
    
    String finalMsg = ""; //"Last Name, Mobile, Country, Email, Phone, Staff Strength, Preferred Model, Work Industry";
    //finalMsg = finalMsg + "\n";
    finalMsg = finalMsg + companyName + "," +  userName + "," + companyAddress + "," + designation + "," + workIndustry + "," + contactNumber + "," + staffStrength + "," + email + "," + prefModel;
    finalMsg = finalMsg + "\n";
    String file = application.getRealPath("/") + "form-data.csv";

    FileWriter filewriter = new FileWriter(file, true);

    filewriter.write(finalMsg);
    filewriter.close();

%>