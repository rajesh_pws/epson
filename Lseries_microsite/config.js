/*
 * Holds the configuration for the entire application.
 * @module CONFIG
 * @public
 */
(function(Epson) {
    // Ie8 polyfill
    window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    var host = window.location.host;
    var patt = new RegExp(".local");
    var result = patt.test(host);

    if (result) {
        Epson.BaseURI = "//epson.local/";
    } else {
        Epson.BaseURI = "//" + host + "/";
    }

    Epson.CONFIG = {
        sendRequest: function(config) {
            var callBackFn = null;
            if (config) {
                var me  = config.scope;
                callBackFn = function(response) {
                    var results = [];
                    if (response) {
                        if (config.callback !== undefined)
                            config.callback.apply(me, [response]);
                    }

                    callBackFn = undefined;
                }
            }

            var jHtml = $(document.getElementsByTagName("html")[0]);
            var url = (config.bUseStaticUrl ? "" : Epson.BaseURI) + config.url;

            if (config.bServerUrl)
            {
                url = Epson.BaseServerURI + config.url;
            }
            var data = config.data || {};
            data.token = jHtml.data("token");

            $.ajax({
                url: url,
                context: document.body,
                dataType: config.dataType || undefined,
                data: data,
                type: config.type || "GET"
            }).done(function(response)
            {
                callBackFn(response);
            }).error(function(response)
            {
            });
        }
    };
    Epson.Utils = {
        IsValue: function(val) {
            if (val !== undefined && val !== null)
            {
                return true;
            }
            else {
                return false;
            }
        },
        RouteTo: function(routeUrl) {
            location.href = "#" + routeUrl;
        },
        randomInt: function(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        IsArray: function(obj) {
            if (obj && Epson.Utils.IsValue(obj.length))
            {
                return true;
            }

            return false;
        },
        IsLandscape: function() {
            if (window.innerWidth > window.innerHeight) {
                return true;
            }
            else
            {
                return false;
            }
        },
        IsTouchDevice: function() {
            if (window.ontouchstart !== undefined) {
                return true;
            }
            else
            {
                return false;
            }
        }
    };

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    window.Epson = Epson;



})(window.Epson || {});

Logger = {};
Logger.log = function(data) {
    console.log(data);
};

//if (window.console && window.console.log)
//{
//    console.log = function() {
//
//    };
//
//    console.warn = function() {
//
//    };
//}

if (true)
{
    window.console = {};
    window.console.log = function() {
    };
    window.console.warn = function() {
    }
}